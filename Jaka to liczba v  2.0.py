print('\n\nW tej grze komputer losuje jedną liczbę z zakresu 0 - 100.\n'
      'Twoim zadaniem jest odgadnięcie tej liczby w jak najmniejszej\n'
      'ilości prób. Zaczynajmy')
print('_' * 20)

tryes = 0

def ask_number(question):
    global tryes
    import random

    random_number = random.randint(0, 101)
    user_number = None

    while not user_number == random_number:
        tryes += 1
        user_number = int(input(question))
        if user_number > random_number:
            print('Twoja liczba jest za duża...')
        elif user_number < random_number:
            print('Twoja liczba jest za mała...')
        elif user_number == random_number:
            print('\n\tGratulacje. Udało Ci się odgadnąć wylosowaną liczbę.')
            return tryes


def main():
    ask_number('Wprowadź liczbę jaką wylosował dla Ciebie komputer: ')
    print('\tPotrzebowałeś', tryes, 'prób.')
    input('\n\nWciśnij Enter żeby zakończyć program')

main()