'''
Zadanie 1. Popraw funkcję ask_number() w taki sposób, aby mogła być wywoływana
z wartością kroku. Spraw aby domyślną wartością kroku była liczba 1.

def ask_for_number(question, lower_number, higher_number):
    response = None
    while response not in range (lower_number, higher_number):
        response = int(input(question))
    return response
'''




def ask_number(question, lower_number, higher_number):
    response = None
    while response not in range (0, 9, 1):
        response = int(input(question))
    return response

#sprawdzenie czy działa funkcja ask_number():
print('Zadanie 1.')
ask_number('Jaką liczbe wybierasz ?: ', 0, 8)






#Zadanie 2. Zmodyfikuj projekt Jaka to liczba ? z rodziału 3. przez użycie w nim funkcji ask_number().

print('Zadanie 2.')
print('\n\nW tej grze komputer losuje jedną liczbę z zakresu 0 - 100.\n'
      'Twoim zadaniem jest odgadnięcie tej liczby w jak najmniejszej\n'
      'ilości prób. Zaczynajmy')
print('_' * 20)

tryes = 0

def ask_number(question):
    global tryes
    import random

    random_number = random.randint(0, 101)
    user_number = None

    while not user_number == random_number:
        tryes += 1
        user_number = int(input(question))
        if user_number > random_number:
            print('Twoja liczba jest za duża...')
        elif user_number < random_number:
            print('Twoja liczba jest za mała...')
        elif user_number == random_number:
            print('\n\tGratulacje. Udało Ci się odgadnąć wylosowaną liczbę.')
            return tryes


def main():
    ask_number('Wprowadź liczbę jaką wylosował dla Ciebie komputer: ')
    print('\tPotrzebowałeś', tryes, 'prób.')
    input('\n\nWciśnij Enter żeby zakończyć program')

main()