
print('W tej grze komputer losuje jedną liczbę z zakresu 0 - 100.\n',
'Twoim zadaniem jest odgadnięcie tej liczby w jak najmniejszej\n',
'ilości prób. Zaczynajmy')
print('_' * 20)


import random

random_number = random.randint(0, 101)

user_number = None
tryes = 0
while user_number != random_number:
    tryes += 1
    user_number = int(input('Wprowadź liczbę wylosowana przez komputer: '))
    if user_number > random_number:
        print('Twoja liczba jest za duża...')
    elif user_number < random_number:
        print('Twoja liczba jest za mała...')
    elif user_number == random_number:
        print('\n\tBrawisimo.. Udało Ci się odganąć wylosowaną liczbę w', tryes, 'próbach.')
        print('\tWylosowana liczba to', random_number)