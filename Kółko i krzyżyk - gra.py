#pseudokod
'''wyświetlić instrukcje gry D
ustalić, do kogo należy 1. ruch D
utworzyć pustą planszę gry
wyświtlić planszę
dopóki nikt nie wygrał lub nie padł remis
    jeśli ruch należy do człowieka
        odczytaj ruch człowieka
        zaktualizuj planszę przez zanaczenie ruchu
    w przeciwnym wypadku
        wyznacz ruch komputerowi
        zaktualizuj planszę przez zaznaczenie ruchu
    wyświetl planszę
    zmień wykonawcę ruchu'''
#Kółko i krzyżyk
#komputer gra przeciwko człowiekowi w kółko i krzyżyk

#stałe globalne:
X = 'X' #gracz\komputer
O = 'O' #gracz\komputer
EMPTY = ' ' #puste pole na planszy
DRAW = 'DRAW' #remis
NUM_SQUARES = 9 #liczba pól na planszy

#wyświetlenie instrukcji do gry
def instruction():
    print('''
    Witaj w największym intelektualnym wyzwanieniem ludzkości jakim jest gra
    Kółko i krzyżyk. Będzie to ostateczna batalia między twoim ludzkim 
    mózgiem a moim krzemowym procesorem.
    Swoje posunięcie wskażesz poprzez wprowadzenie  liczby z 0 - 8.
    Liczba to odpowiada cyfrą na planszy:
    
    0 | 1 | 2
    ---------
    3 | 4 | 5
    ---------
    6 | 7 | 8
    
    Przygotuj się nędzny człowieku. Ostateczne starcie wkrutce się rozpocznie\n''')

#zadaje pytanie na które mozna odpowiedzieć "t" lub "n"(TAK lub NIE):
def ask_yes_no(question):
    response = None
    while response not in ('t', 'n'):
        response = input(question).lower()
    return response

#prosi o podanie liczby z pewnego zakresu:
def ask_for_number(question, lower_number, higher_number):
    response = None
    while response not in range (0, 9, 1):
        response = int(input(question))
    return response

#zadaje pytanie kto ma zacząć pierwszy:
def whos_first():
    go_first = ask_yes_no('Chcesz zaczynać jako pierwszy marna kreaturo ?? ("t"/"n": ')
    if go_first == 't':
        print('Więc zaczynaj jako pierwszy lecz to i tak nic Ci nie da...')
        human = X
        computer = O
    else:
        print('Pozwól ze ja rozpoczne te potyczkę jako pierwszy...')
        computer = X
        human = O
    return computer, human

#tworzy pustą planszę do gry w postaci listy:
def new_board():
    board = []
    for square in range(NUM_SQUARES):
        board.append(EMPTY)
    return board

#wyświetla planszę jako argument:
def display_board(board):
    print('\t', board[0], '|', board[1], '|', board[2])
    print('\t', '-' * 9)
    print('\t', board[3], '|', board[4], '|', board[5])
    print('\t', '-' * 9)
    print('\t', board[6], '|', board[7], '|', board[8])

#otrzymuje plansze jako argument i zwraca listę prawidłowych ruchów:
def legal_moves(board):
    moves = []
    for square in range(NUM_SQUARES):
        if board[square] == EMPTY:
            moves.append(square)
        return moves

#otrzymuje planszę i zwraca wartość okeślającą zwycięzcę:
def winner(board):
    WAYS_TO_WIN = ((0, 1, 2),
                   (3, 4, 5),
                   (6, 7, 8),
                   (0, 3, 6),
                   (1, 4, 7),
                   (2, 5, 8),
                   (0, 4, 8),
                   (2, 4, 6))
    for row in WAYS_TO_WIN:
        if board [row[0]] == board[row[1]] == board[row[2]] != EMPTY:
            winner = board[row[0]]
            return  winner
        if EMPTY not in board:
            return DRAW
        return None

#otrzymuje w wywołaniu planszę i żeton gracza; zwraca numer pola, w którym gracz chce umieścić swój żeton:
def human_move(board, human):
    legal = legal_moves(board)
    move = None
    while move not in legal:
        move  = ask_for_number('Jaki będzie Twój ruch?? (0 - 8):', 0, NUM_SQUARES)
        if move not in legal:
            print('\nTo pole jest już zajęte głupcze. Wybierz inne\n')
            print('Świetnie...')
            return move

def computer_move(board, computer, human):
    #utwórz kopie roboczą, poneważ funkcja będzie zmieniać liste
    board = board[:]
    BEST_MOVES = (4, 0, 2 ,6 ,8 ,1 ,3 ,5 ,7) #najlepsze pozycje do zajęcia według kolejności
    print('Wybieram pole', end='')
    for move in legal_moves(board): #jeśli komputer może wygrać wykonaj te ruch
        board[move] = computer
        if winner(board) == computer:
            print(move)
            return move
        board[move] = EMPTY #ten ruch został sprawdzony , wycofaj go
    for move in legal_moves(board): #jesli gracz może wygrać, zablokuj jego ruch
        board[move] = human
        if winner(board) == human:
            print(move)
            return move
        board[move] = EMPTY #ten tuch został sprawdzony, wycofaj go
    for move in BEST_MOVES: #nikt nie może wygrać w następnym polu; wybierz inne pole
        if move in legal_moves(board):
            print(move)
            return move

#otrzymuje żeton bieżącej kolejki i zwraca żeton następnej kolejki:
def next_turn(turn):
    if turn == X:
        return O
    else:
        return  X

#pogratulowanie zwycięzcy
def congrat_winner(the_winner, computer, human):
    if the_winner != DRAW:
        print(the_winner, 'jest prawdziwym wygranym!!!\n')
    else:
        print('REMIS\n')

    if the_winner == computer:
        print('Jak przypuszczałem nędzniku, jeszcze raz zostałem triufatorem.\n'
              'Wspaniały przykład tego ze AI jest nad istotami żywymi !!!')
    elif the_winner == human:
        print('Nie to nie możliwe!! Jakoś udało Ci się mnie zwieść człowieku..')
    elif the_winner == DRAW:
        print('Miałeś mnóstwo szczęścia człowieku że udało Ci się tylko zremisować ze mną.'
              'Następnym razem ta sytuacja się nie powtórzy...')


#main program:
def main():
    instruction()
    computer, human = whos_first()
    turn = X
    board = new_board()
    display_board(board)

    while not winner(board):
        if turn == human:
            move = human_move(board, human)
            board[move] = human
        else:
            move = computer_move(board, computer, human)
            board[move] = computer
        display_board(board)
        turn = next_turn(turn)

    the_winner = winner(board)
    congrat_winner(the_winner, computer, human)

main()


input('Wciśnij Enter żeby zakończyć program.....')



















