#Odczytaj to
#Demonstruje odczytywanie danych z pliku tekstowego:
print('Otwarcie i następnie zamknięcie pliku...')
print('_' * 40)

text_file = open('odczytaj_to.txt', 'r')
text_file.close()
print('_' * 25)

print('Odczytanie znaków z pliku tekstowego:\n')
text_file = open('odczytaj_to.txt', 'r')
print(text_file.read(1))
print(text_file.read(7))
text_file.close()
print('_' * 25)

print('Odczytanie całego pliku tekstowego:\n')
text_file = open('odczytaj_to.txt', 'r')
wholeTEXT = text_file.read()
print(wholeTEXT)
text_file.close()
print('_' * 25)

print('Odczytanie znaków z wiersza:\n')
text_file = open('odczytaj_to.txt', 'r')
print(text_file.readline(1))
print(text_file.readline(8))
text_file.close()
print('_' * 25)

print('Odczytanie po jednym wierszu na raz:\n')
text_file = open('odczytaj_to.txt', 'r')
print(text_file. readline())
print(text_file.readline())
print(text_file.readline())
text_file.close()
print('_' * 25)

print('Wczytanie całego pliku do listy:\n')
text_file = open('odczytaj_to.txt', 'r')
lines = text_file.readlines()
print(lines)
print(len(lines), ':ilość wierszy w pliku "odczytaj_to.txt"...\n')
for line in lines:
    print(line)
text_file.close()







